﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using WpfTutorial1.Common;
using Xceed.Wpf.Toolkit;

namespace WpfTutorial1.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        #region Private properties

        private bool _isStretch;
        private string _imgPath;
        
        #endregion

        #region Public properties
        
        public ICommand CloseCommand { get; set; }
        public ICommand ClearCommand { get; set; }
        public ICommand ShowCommand { get; set; }
        public Stretch Stretching { get; set; }

        public string ImagePath
        {
            get { return _imgPath; }
            set
            {
                _imgPath = value;
                OnPropertyChanged("ImagePath");
            }
        }

        public bool IsStretch
        {
            get { return _isStretch; }
            set
            {
                _isStretch = value;
                OnPropertyChanged("IsStretch");
                Stretching = value ? Stretch.Uniform : Stretch.None;
                OnPropertyChanged("Stretching");
            }
        }

        #endregion

        #region Command methods

        private void Close()
        {
            this.OnClosingRequest();
        }

        private void Clear()
        {
            ImagePath = null;
        }

        private void Show()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "JPEG Files(*.jpg) |*.jpg| PNG Files(*.png) |*.png| BMP Files(*.bmp) |*.bmp| All files(*.*) |*.*";
            if (openFileDialog.ShowDialog() == true)
            {
                ImagePath = openFileDialog.FileName;
            }
        }

        #endregion

        #region Constructor

        public MainViewModel()
        {
            CloseCommand = new Command(o => Close());
            ClearCommand = new Command(o => Clear());
            ShowCommand = new Command(o => Show());
        }

        #endregion
    }
}
