﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using WpfTutorial1.ViewModels;

namespace WpfTutorial1
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void OnStartup(object sender, StartupEventArgs e)
        {
            try
            {
                // Create the ViewModel and expose it using the View's DataContext
                Views.MainView view = new Views.MainView();
                MainViewModel vm = new MainViewModel();
                vm.ClosingRequest += (sender1, e1) => view.Close();
                view.DataContext = vm;
                view.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
          
        }
    }
}
